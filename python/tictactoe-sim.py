import unittest
from main import check_win
class TestTicTacToe(unittest.TestCase):
    def test_check_win(self):
        board = [['X', 'X', 'X'], [' ', ' ', ' '], [' ', ' ', ' ']]
        self.assertTrue(check_win(board), "Should return True when a row has same elements")

        board = [['X', ' ', ' '], ['X', ' ', ' '], ['X', ' ', ' ']]
        self.assertTrue(check_win(board), "Should return True when a column has same elements")

        board = [['X', ' ', ' '], [' ', 'X', ' '], [' ', ' ', 'X']]
        self.assertTrue(check_win(board), "Should return True when diagonal has same elements")

        board = [[' ', ' ', 'X'], [' ', 'X', ' '], ['X', ' ', ' ']]
        self.assertTrue(check_win(board), "Should return True when reverse diagonal has same elements")

        board = [['X', 'O', 'X'], [' ', 'O', ' '], ['O', ' ', 'X']]
        self.assertFalse(check_win(board), "Should return False when no row, column or diagonal has same elements")

if __name__ == "__main__":
    unittest.main()
