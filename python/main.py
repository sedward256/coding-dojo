#####################
# Welcome to Cursor #
#####################

'''
Step 1: Try generating with Cmd+K or Ctrl+K on a new line. Ask for CLI-based game of TicTacToe.

Step 2: Hit Cmd+L or Ctrl+L and ask the chat what the code does. 
   - Then, try running the code

Step 3: Try highlighting all the code with your mouse, then hit Cmd+k or Ctrl+K. 
   - Instruct it to change the game in some way (e.g. add colors, add a start screen, make it 4x4 instead of 3x3)

Step 4: To try out cursor on your own projects, go to the file menu (top left) and open a folder.
'''
import time  # Importing time for delay in simulation
import colorama  # Importing colorama for colored output
from colorama import Fore, Back, Style  # Importing specific modules from colorama

def menu():  # Function to display the game menu
    while True:  # Infinite loop until user chooses to quit
        print("1. Play game")  # Option to play the game
        print("2. Watch simulation")  # Option to watch a simulation
        print("3. Quit game")  # Option to quit the game
        choice = input("Enter your choice: ")  # User input for choice
        if choice == '1':  # If user chooses to play the game
            tictactoe(False)  # Start the game
        elif choice == '2':  # If user chooses to watch a simulation
            tictactoe(True)  # Start the simulation
        elif choice == '3':  # If user chooses to quit the game
            print("Quitting game...")  # Print quitting message
            break  # Break the loop and end the game
        else:  # If user enters an invalid choice
            print("Invalid choice. Please choose again.")  # Print error message

def print_board_normal(board):  # Function to print the game board in normal mode
    print('-' * 13)  # Print horizontal line
    for row in board:  # For each row in the board
        print("| " + " | ".join(row) + " |")  # Print the row
        print('-' * 13)  # Print horizontal line

def print_board(board):  # Function to print the game board in color mode
    print("+--+--+")  # Print top line
    for row in board:  # For each row in the board
        print("|", end="")  # Print vertical line without newline
        for cell in row:  # For each cell in the row
            if cell == 'X':  # If cell contains 'X'
                print(Back.RED + cell + Style.RESET_ALL, end="|")  # Print 'X' in red
            elif cell == 'O':  # If cell contains 'O'
                print(Back.BLUE + cell + Style.RESET_ALL, end="|")  # Print 'O' in blue
            else:  # If cell is empty
                print(" ", end="|")  # Print empty cell
        print("\n+--+--+")  # Print bottom line

def check_win(board):  # Function to check if a player has won
    for row in board:  # For each row in the board
        if row.count(row[0]) == len(row) and row[0] != ' ':  # If all cells in the row are the same and not empty
            return True  # Return True indicating a win
    for col in range(len(board)):  # For each column in the board
        check = []  # Initialize check list
        for row in board:  # For each row in the board
            check.append(row[col])  # Append the cell in the column to the check list
        if check.count(check[0]) == len(check) and check[0] != ' ':  # If all cells in the column are the same and not empty
            return True  # Return True indicating a win
    if board[0][0] == board[1][1] == board[2][2] and board[0][0] != ' ':  # If all cells in the main diagonal are the same and not empty
        return True  # Return True indicating a win
    if board[0][2] == board[1][1] == board[2][0] and board[0][2] != ' ':  # If all cells in the reverse diagonal are the same and not empty
        return True  # Return True indicating a win
    return False  # If no win condition is met, return False

def tictactoe(simulation=False):  # Function to play the game or run the simulation
    board = [[' ' for _ in range(3)] for _ in range(3)]  # Initialize the game board
    player = 'X'  # Set the starting player to 'X'
    moves = [(0, 0), (0, 1), (0, 2), (1, 0), (1, 1), (1, 2), (2, 0), (2, 1), (2, 2)]  # Predefined moves for simulation
    move_index = 0  # Initialize move index for simulation
    while True:  # Infinite loop until a player wins
        print_board(board)  # Print the game board
        print("Player", player, "turn")  # Print the current player's turn
        if simulation:  # If running a simulation
            time.sleep(1)  # Delay before each move
            row, col = moves[move_index]  # Get the next move from the predefined moves
            move_index += 1  # Increment the move index
        else:  # If playing the game
            try:
                row, col = map(int, input("Enter your move (row col): ").split())  # Get the user's move
            except ValueError:  # If user input is not in correct format
                print("Invalid input. Please enter your move in the format 'row col'.")
                continue
        board[row][col] = player  # Update the game board with the player's move
        if simulation:  # If running a simulation
            time.sleep(1)  # Delay after each move
        if check_win(board):  # If the current player has won
            print_board(board)  # Print the final game board
            print("Player", player, "wins!")  # Print the winning message
            break  # Break the loop and end the game
        player = 'O' if player == 'X' else 'X'  # Switch the player for the next turn

try:
    colorama.init()  # Initialize colorama for colored output
    menu()  # Start the game menu
except Exception as e:  # If an exception occurs
    print("An error occurred: ", e)  # Print the error message
