

def check_digit_9mod(original_number):
    # Step 1: Sum digits
    digit_sum = sum(int(digit) for digit in str(original_number))

    # Step 2: Modulo 9
    check_digit = digit_sum % 9

    # Step 3: Append Check Digit
    result_number = int(str(original_number) + str(check_digit))
    return result_number

def verify_check_digit_9mod(number_with_check):
    # Extract the original number without the check digit
    original_number = int(str(number_with_check)[:-1])

    #Extract the check digit
    check_digit = int(str(number_with_check)[-1])
    # Verify the sum of digits
    digit_sum = sum(int(digit) for digit in str(original_number))

    return digit_sum % 9 == check_digit

def main():
    # Test check_digit_9mod function
    original_number = 123
    result_number = check_digit_9mod(original_number)
    print(f"Result number for {original_number}: {result_number}")

    # Test verify_check_digit_9mod function
    is_valid = verify_check_digit_9mod(result_number)
    print(f"Is valid check digit for {result_number}: {is_valid}")

if __name__ == "__main__":
    main()
